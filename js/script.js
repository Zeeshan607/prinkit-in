jQuery(document).ready(function(){
	jQuery('.next').click(function(){
		jQuery('.nav-tabs > li > .active').parent('.nav-item').next('li').find('a').trigger('click');
	});
	jQuery('.prev').click(function(){
		jQuery('.nav-tabs > li > .active').parent('.nav-item').prev('li').find('a').trigger('click');
	});
	jQuery(document).ready(function(){
		jQuery('.customer-logos').slick({
	        autoplay: true,
	        autoplaySpeed: 0,
	        speed: 2000,
	        cssEase:'linear',
	        focusOnSelect: false,
			slidesToShow: 3.7,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			infinite: true,
			pauseOnHover: false,
			responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 4
				}
			}, {
				breakpoint: 520,
				settings: {
					slidesToShow: 1
				}
			}]
		});
	});
	/* Video Slider - Swiper */
	var videoSlider = new Swiper('.video-slider', {
        pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
		  },
	});
	
// carousel swiper
	// $('.my-carousel').carousel().swipeCarousel({
	// 	sensitivity: 'high',
	// 	});
	// cursor

});
